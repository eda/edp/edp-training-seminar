<h1><center>EDP Training Seminar</center></h1>

## Requirements

- EDP is installed
- Technology Files for `xt018` and `cds` are installed
- `conda` environment is setup
- `.edpinit.json` is configured

If any of these parts are missing, please consult the references below.

## References

- [Installation Guide](https://gitlab-forschung.reutlingen-university.de/eda/edp/edp-quickstart)
- [Handbook](https://gitlab-forschung.reutlingen-university.de/eda/edp/edp-man)
- [Code Examples](https://gitlab-forschung.reutlingen-university.de/eda/edp/edp-py-snippets-xt018)

## General Demonstration

In this demonstration the functionality of EDP will be illustrated by composing
a netlist for the characterization of an NMOS Transistor and generating a gm/ID
look-up table.

## Hands-On Tutorial

In this tutorial we'll go over the core functionality of EDP, using a CMOS
Inverter as demonstrator.

1. A schematic will be created and visualized in the Virtuoso. 
1. A symbol will be created and visualized in the Virtuoso. 
1. A netlist will be created where the symbol is instantiated and simulated.
