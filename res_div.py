import edp

edp_init: str = '../playground/.edpinit.json'
edp_dsgn: str = 'edp_test'
res_div: str  = 'res_div'

# init database
db = edp.Database.init(edp_init)

# create generator
gen = db.createSchematicGenerator(edp_dsgn, res_div, 'schematic')

# show schematic in Schematic Editor
gen.show(True)

# create schematic interactivally
gen.interactive(True)

# no check&Save after each increment
gen.automaticCheck(False)

# access master of resistor
res_master = gen.getSymbolMaster('analogLib', 'res')

# wire length between devices
wire_len: float = 0.25

# no of top and bottom resistors
# no of bottom resistors
top: int = 5
bot: int = 2

# create constraint grouop for resistance
cg = gen.addConstraintGroup();
cg.addParameter('r')

# VIRTUOSO OPENS HERE
# draw pin 'A'
pin  = gen.pin([0, 0], 'A', rotation=3)
elem = pin.wire([0, -wire_len])

# draw top resistors
for i in range(top):
  elem = elem.inst(res_master, anchor='PLUS')
  cg.addInstance(elem)
  elem = elem.wire('MINUS',[0, -wire_len])
  
# draw mid tap pin 'M'
elem.wire([wire_len, wire_len/2]  , route='|-').pin('M', rotation=2)
elem.wire([-wire_len, -wire_len/2], route='-|')

# draw bottom resistors
for i in range(bot):
  elem = elem.inst(res_master, anchor='PLUS')
  cg.addInstance(elem)
  elem = elem.wire('MINUS',[0, -wire_len])
  
# draw pin 'B'
elem.pin('B', rotation=1)

# execute generator
gen.execute()

# run check and save
gen.check()

# Close and open the Database to save everything
_  = edp.Database.quitAll()
db = edp.Database.init(edp_init)

# create symbol generator
gen = db.createReferenceSchematicSymbolGenerator( edp_dsgn
                                                , res_div
                                                , 'schematic'
                                                , edp_dsgn
                                                , res_div
                                                , 'symbol' )
 
# show after execution  
gen.show(True)

# add margin
gen.setLeftMargin(0.5)

# execute generator
gen.execute()

# Close and open the Database to save everything
_  = edp.Database.quitAll()
db = edp.Database.init(edp_init)

#create an empty netlist
tb = db.createNetlist()

# get handle to cell
dut = db.getCell(edp_dsgn, res_div)

# instantiate dut in netlist with connections as dict
tb.addCell('DUT', dut, {'A':'IN', 'B':'gnd!' , 'M': 'OUT'})

# instantiate input voltage with connection as list
indc = tb.addPrimitive('VIN', 'analogLib', 'vdc', ['IN', 'gnd!'])
# specify value
indc.set('vdc', 1.0)

# create a new test
test = db.createTest('spectre', tb)

# get handle to output voltage and save
outnet = test.getNet('OUT')
test.save(outnet)

# add dc operating point analysis (dcop) with name 'op' to test
dcop = test.addAnalysis('dcop', 'op')

# run a simulation save simulation database in variable
sdb = test.simulate()

# get results database of analysis 'op'
rdb = sdb.getResults(dcop)
print(rdb)

# set resistance to R4 to 5kOhm
dut.getView().getInstance('R4').set('r',5e3)

# run and print simulation
sdb = test.simulate()
rdb = sdb.getResults(dcop)
print(rdb)

# get parameter handle of instance
param = dut.getView().getInstance('R4').getParameter('r')

# get all matching groups where the parameter is saved
groups = param.getMatchingGroups()

#get first (and only) matching group in list
group = groups[0]

# remove parameter from matching group
group.removeParameter(param)

# set resistance to R4 to 20kOhm
dut.getView().getInstance('R4').set('r',20e3)

# run and print simulation
sdb = test.simulate()
rdb = sdb.getResults(dcop)
print(rdb)

#quit database
db.quit()
