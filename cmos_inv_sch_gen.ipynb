{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# CMOS-Inverter Expert Design Plan Flow"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "This notebook gives an introduction on how to create an Expert Design Plan (EDP) script in Python. The different sections illustrate the flow from schematic generation over symbol generation to simulation with Cadence Spectre on the example of a CMOS inverter circuit as seen in the picture below.\n",
    "\n",
    "![inverter_circuit](fig/cmos-inv.png)\n",
    "\n",
    "**NOTE:** Before you start, make sure that the kernel finds the EDP package make sure to set the environment variable as shown below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from edp import *\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "from matplotlib import pyplot as plt\n",
    "import plotly\n",
    "import plotly.graph_objs as go\n",
    "from plotly.subplots import make_subplots"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "edp_init = '.edpinit.json'\n",
    "edp_dsgn = 'edp_dsgn'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Schematic Generation\n",
    "\n",
    "The first step in this example is the creation of the schematic for the CMOS inverter circuit. Keep in mind that creating a schematic with EDP might seem cumbersome in contrast to utilzing a graphical editor, but can be worthwile for reusability and if a circuit has a high amount of regularity."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Basic Setup\n",
    "\n",
    "Start by initializing the design database with you `.edpinit.json`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, construct a schematic generator using the `createSchematicGenerator` method from the data base object, given the `edp_dsgn` library and give it an appropriate name for the inverter. Something like `inv` might be appropriate."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The schematic generator can be enabled to visualize the generation process in the virtuoso schematic editor by calling its `show` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similarly, the `interactive` method provides a toggle for interactive generation, meaning the generator will pause after each step."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Check & Save functionality of Virtuoso can be disabled with the `automaticCheck` method, otherwise it will be performed after each increment of the generator."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A Bounding box around the schematic may be visualized with the `setShowBoundingBox` method"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Instatiating Devices\n",
    "\n",
    "The Symbol Master is accessible through the generator object with the `getSymbolMaster` method, given the library and identifier. The NMOS (`ne`) and PMOS (`pe`) in the Xt018 PDK are both located in the `PRIMLIB`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Calls to the `wire` method of a generator object can be succeeded by `pin` or `inst` to create pins or device instances respectively. Pins take a name and an optional keyword argument `direction`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Device instances take the Symbol Master and an optional keyword argument `anchor` in the form of a terminal name."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Connecting Devices\n",
    "\n",
    "The position of any instance is obtained with the `getPosition` method of the corresponding instance. Using the `wire` method instances and pins are connected. The optional `route` keyword argument accepts circuitikz style indicators for the direction. Where `|` indicates only vertical, `--` indicates only horizontal, and `-|` and `|-` indicate horizontal before vertical and vice versa.\n",
    "\n",
    "Start by creating the output routing."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next connecting the PMOS (M0) bulk to $V_{\\mathrm{DD}}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similarly, connect the NMOS (M1) bulk to $V_{\\mathrm{SS}}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Run the Generator\n",
    "\n",
    "A generator is run with the `execute` method. Depending on the `show` and `interactive` settings the progress is visualized in Virtuoso."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Check & Save is called manually with the `check` method. It is good practice to always call this after running a generator even if `automaticCheck` is activated, just to be sure."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Symbol Generation\n",
    "\n",
    "Based on the schematic developed in the previous section, now a symbol is going to be drawn, so that the circuit can be instantiated in a testbench. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For convenience, the EDP toolbox provides a function to create rectangular symbols in a fast manner. It is accesible through the `createRectangularSymbolGenerator` method of the database object. Try constructing a rectangular symbol generator object with a $1.0 \\times 1.5$ box."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As with the schematic generator, the `show` attribute may be set to show the results after execution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The stub length is defined with the `setStubLength` method of the generator object. Let's go with $0.75$ for now."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Pins\n",
    "\n",
    "Pins can be added to the symbol easily with all the information typically available in Cadence Virtuoso. For this the `addPin` method of the generator object is used. It takes a name, a position and optional keyword arguments `direction` and `edge`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Decorations\n",
    "\n",
    "Decorating the symbol is done with generator methods to create special shapes, such as `addPolygon` or `addEllipse`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Run the Generator\n",
    "\n",
    "Once every aspect of the symbol is defined, the symbol generator is executed, just as the schematic generator was before."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Quit the database to make sure everything is saved."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## DC Operating Point Analysis\n",
    "\n",
    "This sections provides an overview of the functions necessary to instantiate the symbol in a testbench netlist and connect it with sources."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Netlist Creation\n",
    "\n",
    "First a netlist is created with the `createNetlist` method of the data base object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Instantiating Cells\n",
    "\n",
    "Each component is placed inside this new netlist. No schematic editor is necessary for the testbench creation. Our inverter is the device under test (DUT) and is retrieved from the data base with the `getCell` method that takes the name of the library and the name of cell.\n",
    "\n",
    "**NOTE:** This only works properly if a both schematic and symbol are accessible."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using the `addCell` method of the testbench, any cell retrieved from the data base is instantiated in the netlist."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Instantiating Primitives and Sources\n",
    "\n",
    "Sources are added to the netlist with the `addPrimitive` method, which takes an arbitrarily chosen identifier, the name of the library, the name of the cell and a list of nets according to the pins of the cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Values are set with the `set` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This can also be combined with the instantiating call of `addPrimitive`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Saving Signals\n",
    "\n",
    "A test is created by providing the simulator (`spectre`) and the previously created testbench netlist as arguments to the `createTest` method of the data base object. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nets are retrieved by the `getNet` method of the created test, and saved with the `save` method of the net"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The current into a terminal is obtained by accessing the instance through the test object using the `getInstance` method and subsequently using the `getTerminal` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Adding The Analysis\n",
    "\n",
    "The DC Operating Point analysis is added with the `addAnalysis` method of the test object, which additionally takes a name."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Running Simulations\n",
    "\n",
    "The simulation is run with the `simulate` method of the test object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Simulation results are retrieved with the `getResults` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Columns of interest are retrieved using the `get` method which accepts either a string or the saved object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This can be encapsulated in a function `sim_vin` that sets the input voltage and runs a simulation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An input voltage sweep from $0\\,\\mathrm{V}$ to  $1.8\\,\\mathrm{V}$ can be easily achieved in a loop."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plotting the Voltage Transfer Characteristic\n",
    "\n",
    "With any plotting library the voltage transfer characteristic can be visualized."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similarly, the current consumption of the inverter is visualized"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Transient Analysis\n",
    "\n",
    "The following code illustrates how to perform a transient analysis of the circuit with EDP. As you can see the steps are similar to the DC operating point analysis in the section above."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Start by creating a new testbench netlist."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next instantiate the DUT."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Pulse Voltage Source\n",
    "\n",
    "Then add the input voltage source. This time it has a period. The `set` method of the primitive object supports the same parameters as the ones available in Virtuoso."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The supply voltage is the same as before."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Create a test as above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This time, save the `region` in addition to the current and the `IN` and `OUT` voltages."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### DC Parameter Sweep\n",
    "\n",
    "Instead of a loop in python, a parameter sweep within the netlist can also be defined using the `dcps` identifier with the `addAnalysis` method of the test object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The resulting analysis object can be modified with the `setParameter` method to specify which parameter is to be swept."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With the `setValues` method the range of values is specified."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Adding the Analysis\n",
    "\n",
    "As above, with the `addAnalysis` method of the test object the transient (`tran`) analysis is added."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The stop time is defined with the `setStopTime` method of the resulting object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For this, the `width` of the NMOS transistor `M1` shall be modified to $10\\,\\mu\\mathrm{m}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Running Simulations\n",
    "\n",
    "The transient simulation analysis is run just as the DC analysis above, with the `simulate` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Likewise, the results are obtained with the `getResults` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Waveforms as Result\n",
    "\n",
    "Unlike above, now the result is a waveform instead of a set of scalars. Each wave form has an $x$ and $y$ component that can be accessed with the `getX` and `getY` methods respectively."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As before, the results may be visualized using any plotting library."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Closing\n",
    "\n",
    "When done with coding an EDP, it is good practice to `quit` the data base connection. This ensures everything is saved. The destructor automatically takes care of this should the data base object leave the scope or when python / the kernel is exited."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
